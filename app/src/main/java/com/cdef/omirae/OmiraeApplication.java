package com.cdef.omirae;

import android.app.Application;
import android.content.Context;
//import android.support.multidex.MultiDex;

import com.cdef.omirae.data.Default;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class OmiraeApplication extends Application {


//    private FirebaseDatabase mDatabase;
//    private DatabaseReference mDBRef;

    public static Context mContext;
    private static Default mDefault;

    public static Default getDefault()
    {

        if(mDefault == null)
            mDefault = new Default();
        return mDefault;
    }

    public static void setDefault(Default input)
    {
        mDefault = input;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.mContext = this;

//        if(mDatabase == null)
//        {
//            this.mDatabase = FirebaseDatabase.getInstance();
//        }

    }
}
