package com.cdef.omirae.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cdef.omirae.R;
import com.cdef.omirae.adapter.HistoryListItem;
import com.cdef.omirae.customView.MessageDialogBuilder;
import com.cdef.omirae.data.History;
import com.cdef.omirae.data.Order;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.NavigationUtils;
import com.cdef.omirae.util.Utils;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;


/***
 *  장바구니/결제 페이지를 보여준다.
 *
 * **/
public class HistoryListActivity extends BaseActivity {
    Utils mUtils = new Utils();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public final int REQUEST_PHONENUMBER = 98;
    public final int REQUEST_ADDRESS = 99;

    public final int REQUEST_OK = 1;
    public final int REQUEST_CANCEL = 0;
    @BindView(R.id.listview)
    ListView mListview;

    FirebaseListAdapter mAdapter;

    DataSetObserver mDataSetObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list);
        ButterKnife.bind(this);
        mDataSetObserver = new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mLoadingView.hide();
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
                mLoadingView.hide();
            }

        };

        getPhoneNumber();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PHONENUMBER: {
                getPhoneNumberProcess();
                break;
            }
        }
    }

    /**
     * 유저의 휴대폰 번호를 불러옴
     ***/
    private void getPhoneNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("회원님의 주문내역은 해당 휴대전화 번호를 기반으로 확인하기때문에 권한 승인을 해주셔야 합니다.")
                    .setDefaultButtons(view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONENUMBER);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    })
                    .complete();

        } else {
            //이미 승인완료 따라서 번호 가져올것.
            getPhoneNumberProcess();
        }
    }

    private String getPhoneNumberProcess() {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");
            setFirebaseDB(myNumber);
            return myNumber;
        } catch (Exception e) {
            return myNumber;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mAdapter != null)
        {
            this.mAdapter.unregisterDataSetObserver(this.mDataSetObserver);
            this.mAdapter.cleanup();
        }
    }

    private void setFirebaseDB(String phone) {
        if (phone != null) {
            if(mAdapter == null)
            {
                this.mLoadingView.show();
                mAdapter = new HistoryListItem(mContext, History.class, R.layout.layout_history_item, this.database.getReference("order").child(phone).orderByChild("state").limitToFirst(100));
                mAdapter.registerDataSetObserver(this.mDataSetObserver);
                mListview.setAdapter(mAdapter);
                mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        NavigationUtils.goHistoryDetailActivity(mContext,                         ((History)mAdapter.getItem(i))
                        );
                    }
                });

            }


        } else {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("해당 기기의 전화번호를 불러올 수 없어 목록을 가져올 수 없습니다.")
                    .setDefaultButtons(view -> {
                        MessageDialogBuilder.getInstance(this).dismiss();
                        finish();
                    })
                    .complete();
        }

    }
}
