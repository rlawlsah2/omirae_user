package com.cdef.omirae.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.cdef.omirae.OmiraeApplication;
import com.cdef.omirae.R;
import com.cdef.omirae.data.Default;
import com.cdef.omirae.util.LogUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rw.loadingdialog.LoadingView;

/**
 * Created by kimjinmo on 2017. 8. 24..
 */

public class BaseActivity extends Activity {







    protected LoadingView mLoadingView;


    protected Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        if(getWindow().getDecorView().getRootView() != null)
        {
            this.mLoadingView = new LoadingView.Builder(this)
                    .setProgressColorResource(R.color.Default)
                    .setBackgroundColorRes(R.color.dim)
                    .setProgressStyle(LoadingView.ProgressStyle.CYCLIC)
                    .setCustomMargins(0,0,0,0)
                    .attachTo(this);
        }


    }

    @Override
    protected void onDestroy() {
        if(mLoadingView != null)
            this.mLoadingView.hide();
        super.onDestroy();
        Runtime.getRuntime().gc();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }



    protected void initDefault() {

        /**
         * 초기값 셋팅팅
         * **/
        FirebaseDatabase.getInstance().getReference().child("default")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        LogUtil.d("시스템 정보 : " + dataSnapshot);
                        if (dataSnapshot != null) {
                            OmiraeApplication.setDefault(dataSnapshot.getValue(Default.class));
                            settingRefresh();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    protected void settingRefresh()
    {

    }
}
