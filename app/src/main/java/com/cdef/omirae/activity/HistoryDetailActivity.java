package com.cdef.omirae.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cdef.omirae.R;
import com.cdef.omirae.customView.OrderedMenuListItem;
import com.cdef.omirae.data.History;
import com.cdef.omirae.data.OrderedMenu;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;


/***
 *  주문내역 상세내용을 보여준다.
 * **/
public class HistoryDetailActivity extends BaseActivity {
    public final int REQUEST_PHONENUMBER = 98;

    FirebaseDatabase database;// = FirebaseDatabase.getInstance();
    DatabaseReference myRef;// = database.getReference("cart");
    @BindView(R.id.layout_order_list)
    LinearLayout mLayoutOrderList;
    @BindView(R.id.text_price)
    TextView mTextPrice;
    @BindView(R.id.text_address)
    TextView mTextAddress;
    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.button_order)
    Button mButtonOrder;
    @BindView(R.id.text_state)
    TextView mTextState;
    @BindView(R.id.text_state_sub)
    TextView mTextStateSub;
    @BindView(R.id.text_memo)
    TextView mTextMemo;
    @BindView(R.id.text_delivery_fee)
    TextView mTextDeliveryFee;
    @BindView(R.id.layout_address)
    LinearLayout mLayoutAddress;


    private History mHistory;
    private Utils mUtils = new Utils();
    private long mTotalPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_datail);
        ButterKnife.bind(this);
        this.mContext = this;


        /**
         * Intent로 넘겨받은 데이터를 파싱해야 함
         * **/

        Intent bundle = getIntent();
        LogUtil.d("주문내역 상세보기 bundle : " + bundle);
//        LogUtil.bundle2string(bundle.getSerializableExtra("history"));

        if (bundle != null) {
            mHistory = (History) bundle.getSerializableExtra("history");
            LogUtil.d("주문내역 상세보기 mHistory : " + mHistory);
            LogUtil.d("주문내역 상세보기 mHistory.state : " + mHistory.state);
            LogUtil.d("주문내역 상세보기 mHistory.type : " + mHistory.delivery);

            ///셋팅
            this.mTextMemo.setText(mHistory.memo);
            this.mScrollView.setEnabled(true);
            switch ((int) mHistory.state) {

                case 1:
                    this.mTextState.setText("주문 접수중");
                    this.mTextState.setTextColor(Color.parseColor("#DF4D4D"));
                    break;
                case 0:
                    this.mTextState.setText("주문 확인 및 처리중");
                    this.mTextState.setTextColor(Color.parseColor("#DF4D4D"));

                    break;
                case 2:
                    this.mTextState.setText("배달 완료");
                    this.mTextState.setTextColor(Color.parseColor("#000000"));

                    break;
            }


            switch ((int) mHistory.delivery) {

                case 0:
                    this.mTextAddress.setText(mHistory.address);
                    this.mTextDeliveryFee.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    this.mTextAddress.setText("방문 포장");
                    this.mTextDeliveryFee.setVisibility(View.GONE);
                    break;
                case 2:
                    this.mTextAddress.setText("매장 식사");
                    this.mTextDeliveryFee.setVisibility(View.GONE);
                    break;
            }

            for (OrderedMenu menu : mHistory.menu) {
                LogUtil.d("@넘겨받은 데이터를 확인해보자 : " + menu.title);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.count);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.description);
                LogUtil.d("넘겨받은 데이터를 확인해보자 : " + menu.price);
                OrderedMenuListItem newItem = new OrderedMenuListItem(mContext);
                newItem.setData(menu.title, menu.count, mUtils.setComma(menu.price) + "원");
                this.mLayoutOrderList.addView(newItem);
                this.mTotalPrice += menu.price * menu.count;
            }

            if (mHistory.delivery == 0) {
                this.mTotalPrice += mHistory.deliveryFee;
                this.mTextPrice.setText("* " + mUtils.setComma(this.mTotalPrice) + "원");
            } else
                this.mTextPrice.setText(mUtils.setComma(this.mTotalPrice) + "원");


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99) {
            if (resultCode == 0) {
                //그냥 취소인경우
            } else if (resultCode == 1) {
                myRef.removeValue();
                finish();
            }
        }
    }

}
