package com.cdef.omirae.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cdef.omirae.OmiraeApplication;
import com.cdef.omirae.R;
import com.cdef.omirae.customView.MenuListHeader;
import com.cdef.omirae.customView.MenuListItem;
import com.cdef.omirae.customView.MessageDialogBuilder;
import com.cdef.omirae.data.Default;
import com.cdef.omirae.data.Menu;
import com.cdef.omirae.data.User;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.NavigationUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/***
 *  메인페이지. 메뉴목록을 보여준다.
 *
 * **/
public class MainActivity extends BaseActivity {


    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("menu");
    @BindView(R.id.layout_scroll)
    LinearLayout mLayoutScroll;
    @BindView(R.id.scrollview)
    ScrollView mScrollview;
    Context mContext;
    @BindView(R.id.text_state)
    TextView mTextState;
    @BindView(R.id.layout_state)
    LinearLayout mLayoutState;
    @BindView(R.id.button_history)
    Button mButtonHistory;
    @BindView(R.id.layout_header)
    MenuListHeader mLayoutHeader;

    @OnClick(R.id.layout_state)
    void goCart(View v) {
        NavigationUtils.goCartActivity(this);
    }


    private ArrayList<MenuListItem> mItemList = new ArrayList<>();


    @Override
    protected void onResume() {
        super.onResume();
        // 최근 주문내역중 S 상태인것이 있을경우 표기
        ///DB 접근해서 데이터 가져오기
        if (getPhoneNumberProcess() != null)
//            this.database.getReference("order").child(getPhoneNumberProcess()).orderByValue().
            this.database.getReference("cart").child(getPhoneNumberProcess()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getChildrenCount() >= 1) {
                        ///알림
                        mLayoutState.setVisibility(View.VISIBLE);
                        mTextState.setSelected(true);
                    } else {
                        mLayoutState.setVisibility(View.GONE);
                        mTextState.setSelected(false);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    LogUtil.d("에러나나 : " + databaseError);

                }
            });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.mContext = this;
        initDefault();
        this.mLoadingView.show();


        ///DB 접근해서 데이터 가져오기
        this.myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("kk9991", "어디보자0 : " + dataSnapshot);
                Menu tmp1 = null;
                Menu tmp2 = null;

                int index = 0;
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Log.d("kk9991", "어디보자1 : " + data);
                    Menu tmp = data.getValue(Menu.class);

                    if ((index % 2) == 0) {
                        ///첫 뷰 생성 구역
                        MenuListItem layout = new MenuListItem(mContext);
                        layout.setData(tmp);
                        mItemList.add(layout);
                        mLayoutScroll.addView(layout);

                    } else {
                        mItemList.get((index / 2)).setData(tmp);
                    }
                    index++;
                }

                mLoadingView.hide();
                getPhoneNumber();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        this.myRef.child("menu").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.d("kk9991" , "어디보자2 : " + dataSnapshot);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });


//        this.myRef.child("menu").addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                Log.d("kk9991", "메뉴 정보를 가져와봤습니다 dataSnapshot: " + dataSnapshot );
//                Log.d("kk9991", "메뉴 정보를 가져와봤습니다 s: " + s );
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }


    ////
    private void updateUserInfo() {
        String token = FirebaseInstanceId.getInstance().getToken();
        LogUtil.d("token : " + token);

        if (getPhoneNumberProcess() != null) {
            if (token != null) {
                User user = new User();
                user.token = token;
                FirebaseDatabase.getInstance().getReference("user").child(getPhoneNumberProcess()).setValue(user);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 99: {
                updateUserInfo();
//                getPhoneNumberProcess();
                break;
            }
        }
    }


    /**
     * 유저의 휴대폰 번호를 불러옴
     ***/
    private void getPhoneNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("회원님의 주문내역을 확인을 위해 사용자 인증이 필요합니다. 확인을 눌러 승인해주세요.")
                    .setDefaultButtons(view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 99);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    })
                    .complete();

        } else {
            //이미 승인완료 따라서 번호 가져올것.
//            getPhoneNumberProcess();
            updateUserInfo();
        }
    }

    private String getPhoneNumberProcess() {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");
            return myNumber;
        } catch (NullPointerException e) {
            return myNumber;
        } catch (SecurityException e) {
            return myNumber;
        }
    }

    @Override
    protected void settingRefresh() {
        super.settingRefresh();
        mLayoutHeader.setImage(OmiraeApplication.getDefault().homePic);

    }
}
