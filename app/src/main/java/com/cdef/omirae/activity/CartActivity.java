package com.cdef.omirae.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.omirae.R;
import com.cdef.omirae.customView.CartItemLayout;
import com.cdef.omirae.customView.MenuListItem;
import com.cdef.omirae.customView.MessageDialogBuilder;
import com.cdef.omirae.customView.OrderedMenuListItem;
import com.cdef.omirae.data.Cart;
import com.cdef.omirae.data.Menu;
import com.cdef.omirae.data.OrderedMenu;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.NavigationUtils;
import com.cdef.omirae.util.Utils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/***
 *  장바구니 페이지를 보여준다.
 *
 * **/
public class CartActivity extends BaseActivity implements ChildEventListener{


    private HashMap<Long, Cart> mCartList = new HashMap<>();

    FirebaseDatabase database;
    DatabaseReference myRef;
    Utils mUtils = new Utils();
    @BindView(R.id.button_all_check)
    Button mButtonAllCheck;
    @BindView(R.id.text_count)
    TextView mTextCount;
    @BindView(R.id.button_all_clear)
    TextView mButtonAllClear;
    @BindView(R.id.button_buy)
    Button mButtonBuy;
    @OnClick(R.id.button_buy)
    void goBuy(View v)
    {
        if(this.mCartList != null && this.mCartList.size() > 0)
        {
            NavigationUtils.goOrderActivity(this);
        }
        else
        {
            Toast.makeText(mContext, "장바구니가 비어있습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.button_all_clear)
    void allClear(View v) {
//        if (this.mCartUtils.getCartList().size() > 0)
//            MessageDialogBuilder.getInstance(mContext).setTitle("장바구니 전체삭제")
//                    .setContent("장바구니에 있는 " + mCartUtils.getCartList().size() + "개의 내역을 삭제하시겠습니까?")
//                    .setDefaultButtons(view -> {
//                        this.mCartUtils.deleteAllCart(new RealmChangeListener() {
//                            @Override
//                            public void onChange(Object element) {
//                                setItems();
//                            }
//                        });
//                        MessageDialogBuilder.getInstance(mContext).dismiss();
//                    })
//                    .complete();
    }

    @BindView(R.id.text_total_price)
    TextView mTextTotalPrice;
    @BindView(R.id.layout_list)
    LinearLayout mLayoutList;

    private int mTotalPrice;
    private boolean misCompleteValidate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        //휴대폰 번호를 가져옴.
        if(getPhoneNumberProcess() != null)
        {
            this.mLoadingView.show();
            database = FirebaseDatabase.getInstance();
            myRef = database.getReference("cart").child(getPhoneNumberProcess());

            this.mLayoutList.removeAllViews();
            ///DB 접근해서 데이터 가져오기
            this.myRef.addChildEventListener(this);
            this.mLoadingView.hide();

        }
        else
        {
            //전화번호 인증 안되니까 못불러옴.
            MessageDialogBuilder.getInstance(mContext).setTitle("인증실패")
                    .setContent("전화번호를 확인할 수 없어 장바구니를 조회할 수 없습니다.")
                    .setOkButton("닫기", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            MessageDialogBuilder.getInstance(mContext).dismiss();
                            finish();
                        }
                    })
                    .complete();
            this.mLoadingView.hide();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        myRef.removeEventListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 99)
        {
            if(resultCode == 0)
            {
                //그냥 취소인경우
            }
            else if(resultCode == 1)
            {
                myRef.removeValue();
                finish();
            }
        }
    }


    private void setItems(DataSnapshot root) {
        this.mLayoutList.removeAllViews();
        for (DataSnapshot item : root.getChildren()) {
            Cart tmp = item.getValue(Cart.class);
            this.mCartList.put(tmp.seq, tmp);

            CartItemLayout cartItemLayout = new CartItemLayout(this);
            cartItemLayout.setTag(tmp.seq);
            cartItemLayout.setData(tmp);
            cartItemLayout.setButtonClickListener(new CartItemLayout.ButtonClickListener() {
                @Override
                public void onCountChange(long seq, int count) {
                    mCartList.get(seq).count = count;
                    myRef.child(seq+"").setValue(mCartList.get(seq));
                    calcTotalPrice();

                }

                @Override
                public void onDelete(Cart cart) {
                    myRef.child(cart.seq+"").removeValue();
                }
            });
            this.mLayoutList.addView(cartItemLayout);
        }

        calcTotalPrice();

    }

    @Override
    protected void onDestroy() {
        //1. firebase 관련 초기화
        super.onDestroy();
    }

    private void setItem(DataSnapshot item) {

        Cart tmp = item.getValue(Cart.class);
        this.mCartList.put(tmp.seq, tmp);

        CartItemLayout cartItemLayout = new CartItemLayout(this);
        cartItemLayout.setTag(tmp.seq);
        cartItemLayout.setData(tmp);
        cartItemLayout.setButtonClickListener(new CartItemLayout.ButtonClickListener() {
            @Override
            public void onCountChange(long seq, int count) {
                mCartList.get(seq).count = count;
                myRef.child(seq+"").setValue(mCartList.get(seq));
                calcTotalPrice();

            }

            @Override
            public void onDelete(Cart cart)
            {
                mLoadingView.show();
                myRef.child(cart.seq+"").removeValue();
            }
        });
        this.mLayoutList.addView(cartItemLayout);

        calcTotalPrice();
        this.mLoadingView.hide();


    }

    private void calcTotalPrice()
    {
        this.mTotalPrice = 0;
        for(long cart : this.mCartList.keySet())
        {
            LogUtil.d("메뉴 가격 확인");
            LogUtil.d("메뉴 명 : " + this.mCartList.get(cart).title);
            LogUtil.d("메뉴 가격 : " + this.mCartList.get(cart).price);
            LogUtil.d("메뉴 수량 : " + this.mCartList.get(cart).count);
            this.mTotalPrice += (this.mCartList.get(cart).price * this.mCartList.get(cart).count);
        }
        this.mTextTotalPrice.setText(this.mUtils.setComma(this.mTotalPrice));
    }


    private String getPhoneNumberProcess()
    {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try{
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");

            return myNumber;

        }catch(Exception e){
            return myNumber;
        }

    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        LogUtil.d("장바구니 onChildAdded");
        if(dataSnapshot != null)
        {
            setItem(dataSnapshot);
        }
        else
        {
            this.mLoadingView.hide();

        }

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        LogUtil.d("장바구니 onChildChanged");
        this.mLoadingView.hide();

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        LogUtil.d("장바구니 onChildRemoved");
        Cart tmp = dataSnapshot.getValue(Cart.class);


        ///삭제된 경우
        ///1. mDataList 에서 삭제
        mCartList.remove(tmp.seq);
        ///2. mLayoutList 에서 삭제
        for(int i=0; i < mLayoutList.getChildCount(); i++)
        {
            if(((CartItemLayout)mLayoutList.getChildAt(i)).getTag() != null)
                if(tmp.seq == (long)((CartItemLayout)mLayoutList.getChildAt(i)).getTag())
                {
                    mLayoutList.removeViewAt(i);
                    calcTotalPrice();
                    break;
                }
        }
        this.mLoadingView.hide();


    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        LogUtil.d("장바구니 onChildMoved");
        this.mLoadingView.hide();


    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        LogUtil.d("장바구니 onCancelled");
        this.mLoadingView.hide();


    }
}
