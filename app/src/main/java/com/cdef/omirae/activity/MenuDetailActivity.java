package com.cdef.omirae.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.cdef.omirae.R;
import com.cdef.omirae.customView.CountButton;
import com.cdef.omirae.customView.MenuListItem;
import com.cdef.omirae.customView.MessageDialogBuilder;
import com.cdef.omirae.data.Cart;
import com.cdef.omirae.data.Menu;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.NavigationUtils;
import com.cdef.omirae.util.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/***
 *  메뉴 상세내을 보여준다.
 * **/
public class MenuDetailActivity extends BaseActivity {
    public final int REQUEST_PHONENUMBER = 98;

    FirebaseDatabase database;// = FirebaseDatabase.getInstance();
    DatabaseReference myRef;// = database.getReference("cart");

    @BindView(R.id.image)
    ImageView mImage;
    @BindView(R.id.layout_info)
    LinearLayout mLayoutInfo;
    @BindView(R.id.layout_buttons)
    LinearLayout mLayoutButtons;
    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.text_price)
    TextView mTextPrice;
    @BindView(R.id.count_button)
    CountButton mCountButton;
    @BindView(R.id.button_cart)
    Button mButtonCart;
    @OnClick(R.id.button_cart)
    void goCart(View v)
    {
        LogUtil.d("장바구니에 담기");
        getPhoneNumber();
    }
    @BindView(R.id.button_order)
    Button mButtonOrder;
    @OnClick(R.id.button_order)
    void goOrder(View v)
    {
        LogUtil.d("주문하기");
        NavigationUtils.goOrderActivity(mContext, this.mMenu, this.mCountButton.getCount());

    }

    private Menu mMenu;
    private ArrayList<MenuListItem> mItemList = new ArrayList<>();
    private Utils mUtils = new Utils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_datail);
        ButterKnife.bind(this);
        this.mContext = this;


        /**
         * Intent로 넘겨받은 데이터를 파싱해야 함
         * **/

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mMenu = new Menu();
            mMenu.seq = bundle.getLong("seq");
            mMenu.title = bundle.getString("title");
            mMenu.description = bundle.getString("description");
            mMenu.price = bundle.getLong("price");
            mMenu.etc = bundle.getString("etc");
            mMenu.image = bundle.getString("image");


            this.mTextTitle.setText(mMenu.title);
            this.mTextPrice.setText(mUtils.setComma(mMenu.price) + "원");

            this.mCountButton.setListener(new CountButton.eventListener() {
                @Override
                public void countChange() {

                }
            });


            Glide.with(this).load(mMenu.image).asBitmap()                    .override(mUtils.dpToPx(this, 100), mUtils.dpToPx(this, 100))

                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            mImage.setImageBitmap(resource);
                        }
                    });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case REQUEST_PHONENUMBER :
            {
                getPhoneNumberProcess();
                break;
            }
        }
    }



    /**
     * 유저의 휴대폰 번호를 불러옴
     *
     * ***/
    private void getPhoneNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("장바구니 기능은 회원님의 휴대전화 번호를 기반으로 기록되기 때문에 권한 승인이 필요합니다.")
                    .setDefaultButtons(view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONENUMBER);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    })
                    .complete();

        }
        else
        {
            //이미 승인완료 따라서 번호 가져올것.
            getPhoneNumberProcess();
        }
    }
    private void getPhoneNumberProcess()
    {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try{
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");
//            this.mEditPhone.setText(myNumber+"");

            /**
             * 장바구니에 추가하기.
             * **/
            this.addCart(myNumber);

        }catch(Exception e){

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 99)
        {
            if(resultCode == 0)
            {
                //그냥 취소인경우
            }
            else if(resultCode == 1)
            {
//                FirebaseDatabase.getInstance().getReference("cart").removeValue();
                finish();
            }
        }
    }

    private void addCart(String phone)
    {

        this.mLoadingView.show();
        this.database = FirebaseDatabase.getInstance();
        this.myRef = this.database.getReference("cart");    //cart에 접근하는 reference를 얻는다.

        if(phone != null)
        {
            ///1. 메뉴정보 가져오기
            Cart cart = new Cart();
            cart.seq = this.mMenu.seq;
            cart.title = this.mMenu.title;
            cart.description = this.mMenu.description;
            cart.price = this.mMenu.price;
            cart.image = this.mMenu.image;
            cart.etc = this.mMenu.etc;
            ///2. 수량정보 가져오기
            cart.count = this.mCountButton.getCount();
            ///3. 데이터베이스 저장

            this.myRef.child(phone).child(cart.seq+"").runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {
                    mutableData.setValue(cart);
                    return Transaction.success(mutableData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                    Toast.makeText(mContext, "장바구니에 추가되었습니다.", Toast.LENGTH_SHORT).show();
                    mLoadingView.hide();
                }
            });
//            this.myRef.child(phone).child(cart.seq+"").setValue(cart);
        }

    }


}
