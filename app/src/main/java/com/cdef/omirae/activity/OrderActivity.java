package com.cdef.omirae.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.omirae.OmiraeApplication;
import com.cdef.omirae.R;
import com.cdef.omirae.customView.CheckOrderDialogBuilder;
import com.cdef.omirae.customView.MessageDialogBuilder;
import com.cdef.omirae.customView.OrderedMenuListItem;
import com.cdef.omirae.data.Cart;
import com.cdef.omirae.data.OrderState;
import com.cdef.omirae.data.OrderedMenu;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/***
 *  장바구니/결제 페이지를 보여준다.
 *
 * **/
public class OrderActivity extends BaseActivity {

    public final int REQUEST_PHONENUMBER = 98;
    public final int REQUEST_ADDRESS = 99;

    public final int REQUEST_OK = 1;
    public final int REQUEST_CANCEL = 0;




    ArrayList<OrderedMenu> mOrderList = new ArrayList<>();
    @BindView(R.id.image_button1)
    ImageView mImageButton1;
    @BindView(R.id.layout_button1)
    LinearLayout mLayoutButton1;
    @BindView(R.id.image_button2)
    ImageView mImageButton2;
    @BindView(R.id.layout_button2)
    LinearLayout mLayoutButton2;
    @BindView(R.id.image_button3)
    ImageView mImageButton3;
    @BindView(R.id.layout_button3)
    LinearLayout mLayoutButton3;
    @BindView(R.id.text_button1)
    TextView mTextButton1;
    @BindView(R.id.text_button2)
    TextView mTextButton2;
    @BindView(R.id.text_button3)
    TextView mTextButton3;
    @BindView(R.id.layout_delivery_info)
    LinearLayout mLayoutDeliveryInfo;
    @BindView(R.id.text_delivery_fee)
    TextView mTextDeliveryFee;

    @OnClick({R.id.layout_button1, R.id.layout_button2, R.id.layout_button3})
    void selectItem(View v) {
        if (v == mLayoutButton1) {
            this.mImageButton1.setSelected(true);
            this.mImageButton2.setSelected(false);
            this.mImageButton3.setSelected(false);

            this.mTextButton1.setTextColor(getResources().getColor(R.color.DefaultPressed));
            this.mTextButton2.setTextColor(getResources().getColor(R.color.darkGray));
            this.mTextButton3.setTextColor(getResources().getColor(R.color.darkGray));

        } else if (v == mLayoutButton2) {
            this.mImageButton2.setSelected(true);
            this.mImageButton1.setSelected(false);
            this.mImageButton3.setSelected(false);

            this.mTextButton2.setTextColor(getResources().getColor(R.color.DefaultPressed));
            this.mTextButton1.setTextColor(getResources().getColor(R.color.darkGray));
            this.mTextButton3.setTextColor(getResources().getColor(R.color.darkGray));
        } else if (v == mLayoutButton3) {
            this.mImageButton3.setSelected(true);
            this.mImageButton1.setSelected(false);
            this.mImageButton2.setSelected(false);

            this.mTextButton3.setTextColor(getResources().getColor(R.color.DefaultPressed));
            this.mTextButton2.setTextColor(getResources().getColor(R.color.darkGray));
            this.mTextButton1.setTextColor(getResources().getColor(R.color.darkGray));
        }
        clickItemProcess();
    }


    private boolean mFlagTwoOrdered = false;

    @BindView(R.id.edit_address)
    EditText mEditAddress;
    @BindView(R.id.edit_phone)
    EditText mEditPhone;
    @OnClick(R.id.edit_phone)
    void clickPhone(View v)
    {
        Toast.makeText(mContext, "연락처는 해당 휴대전화 번호만 입력 가능합니다.", Toast.LENGTH_SHORT).show();
    }
    @BindView(R.id.layout_user_info)
    LinearLayout mLayoutUserInfo;
    @BindView(R.id.text_price)
    TextView mTextPrice;
    @BindView(R.id.button_order)
    Button mButtonOrder;
    @BindView(R.id.edit_memo)
    EditText mEditMemo;

    @OnClick(R.id.button_order)
    void goOrder(View v) {
        /////주문 전 체크할 것
        /**
         * 영업시간 안내
         * **/
        if(OmiraeApplication.getDefault().open == 0)
        {

            MessageDialogBuilder.getInstance(mContext)
                    .setTitle("영업시간 종료")
                    .setContent("영업시간이 종료되어 주문할 수 없습니다.\n\n" + OmiraeApplication.getDefault().openSub)
                    .onDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            finish();
                        }
                    })
                    .setOkButton("확인", view -> {
                        MessageDialogBuilder.getInstance(mContext).dismiss();
                    })
                    .complete();
            return;
        }

        if (!this.mFlagTwoOrdered) {
            this.mLoadingView.show();

            //1. 주소지 확인
            String address = this.mEditAddress.getText().toString();
            if (address == null || address.length() < 5) {
                Toast.makeText(this, "주소지를 확인해주세요.", Toast.LENGTH_SHORT).show();
                return;
            }
            //2. 연락처 확인
            String phone = this.mEditPhone.getText().toString();
            if (phone == null || phone.length() < 5) {
                Toast.makeText(this, "정확하지 않은 전화번호 기재시 주문이 취소될 수 있습니다.", Toast.LENGTH_SHORT).show();
                return;
            }
            //3. 최종 결제금액 확인
            if (calcTotalPrice() <= 0) {
                Toast.makeText(this, "주문내역을 불러올 수 없습니다.", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            ///3.5 메모 추가하기
            String memo = this.mEditMemo.getText().toString();

            ///3.75 주문 유형 추가에 따른 타입 전송송
            long deliveryType = (this.mImageButton1.isSelected() ? 0 : (this.mImageButton2.isSelected()? 1: 2));


            CheckOrderDialogBuilder.getInstance(mContext).setContent(
                    deliveryType,
                    address,
                    phone
            )
                    .onDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            mLoadingView.hide();
                        }
                    })
                    .setCancelButton("취소", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CheckOrderDialogBuilder.getInstance(mContext).dismiss();
                        }
                    })
                    .setOkButton("주문", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CheckOrderDialogBuilder.getInstance(mContext).dismiss();
                            //4. 이상없으므로 주문서 보내기 진행하자
                            String timeStamp = mUtils.getCurrentTime();
                            database.getReference("order").child(phone).child(timeStamp).runTransaction(new Transaction.Handler() {
                                @Override
                                public Transaction.Result doTransaction(MutableData mutableData) {

                                    //(0). state 등록
                                    OrderState state = new OrderState();
                                    state.phone = phone;
                                    state.delivery = deliveryType;
                                    if(state.delivery == 0)
                                    {
                                        state.address = address;
                                        state.deliveryFee = OmiraeApplication.getDefault().deliveryFee;
                                    }
                                    state.time = timeStamp;
                                    state.memo = memo;
                                    mutableData.setValue(state);
                                    //(1). order 등록
//                Order order = new Order();
//                order.address = address;
//                order.time = timeStamp;
//                mutableData.child("history").setValue(order);

                                    //(2). orderMenu 등록
                                    for (OrderedMenu menu : mOrderList) {
                                        mutableData.child("menu").child(menu.seq + "").setValue(menu);
                                    }

                                    return Transaction.success(mutableData);
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                                    if (databaseError == null) {

                                        String notiContent;
                                        if (mOrderList.size() == 1) {
                                            notiContent = mOrderList.get(0).title + " 주문이 완료되었습니다.";
                                        } else {
                                            notiContent = mOrderList.get(0).title + " 외 " + (mOrderList.size() - 1) + "건 주문이 완료되었습니다";
                                        }

                                        Utils.orderComplete(mContext, notiContent);
                                        LogUtil.d("주문 결과를 확인합니다 databaseError: " + databaseError);
                                        LogUtil.d("주문 결과를 확인합니다 b: " + b);
                                        Toast.makeText(mContext, "주문 완료", Toast.LENGTH_SHORT).show();
                                        setResult(1);
                                        finish();
                                    } else {
                                        MessageDialogBuilder.getInstance(mContext)
                                                .setTitle("알림")
                                                .setContent("주문실패. 다시 주문해주세요")
                                                .setOkButton("확인", view -> finish())
                                                .complete();
                                    }
                                }
                            });
                        }
                    })
                    .complete();









        } else {
            mLoadingView.hide();
            MessageDialogBuilder.getInstance(mContext)
                    .setTitle("알림")
                    .setContent("이미 접수된 주문이 2건을 초과하여 더이상 주문할 수 없습니다.")
                    .setOkButton("확인", view -> {
                        MessageDialogBuilder.getInstance(mContext).dismiss();
                        finish();
                    })
                    .complete();
        }
    }

    @BindView(R.id.layout_price)
    LinearLayout mLayoutPrice;
    @BindView(R.id.layout_receipt)
    LinearLayout mlayoutReceipt;
    Utils mUtils = new Utils();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private long mDeliveryFee = OmiraeApplication.getDefault().deliveryFee;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        initDefault();

        /**
         * 영업시간 안내
         * **/
        if(OmiraeApplication.getDefault().open == 0)
        {

            MessageDialogBuilder.getInstance(mContext)
                    .setTitle("영업시간 종료")
                    .setContent("영업시간이 종료되어 주문할 수 없습니다.\n" + OmiraeApplication.getDefault().openSub)
                    .onDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            finish();
                        }
                    })
                    .setOkButton("확인", view -> {
                        MessageDialogBuilder.getInstance(mContext).dismiss();
                    })
                    .complete();
            return;
        }


        this.mLayoutButton1.performClick();
        this.mLoadingView.show();


        ///bundle 체크
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            if (bundle.getString("type").equals("direct")) {
                ///바로 주문하기인가?
                OrderedMenu menu = new OrderedMenu();
                menu.title = bundle.getString("title");
                menu.description = bundle.getString("description");
                menu.price = bundle.getLong("price");
                menu.image = bundle.getString("image");
                menu.etc = bundle.getString("etc");
                menu.count = bundle.getInt("count");
                this.mOrderList.add(menu);
                setOrderedListView();
                getPhoneNumber();
//                getMyLocation();

                this.mTextPrice.setText((this.mImageButton1.isSelected() ? "* " : "") +this.mUtils.setComma(calcTotalPrice()) + "원");
            } else {
                //장바구니에서 넘어왔음. DB를 뒤져보자
                ///DB 접근해서 데이터 가져오기
                this.database.getReference("cart").child(getPhoneNumberProcess()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d("kk9991", "어디보자0 : " + dataSnapshot);

                        int index = 0;
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            Log.d("kk9991", "어디보자1 : " + data);
                            if (data != null) {
                                Cart tmp = data.getValue(Cart.class);
                                OrderedMenu newOrderedMenu = new OrderedMenu();
                                newOrderedMenu.seq = tmp.seq;
                                newOrderedMenu.title = tmp.title;
                                newOrderedMenu.description = tmp.description;
                                newOrderedMenu.image = tmp.image;
                                newOrderedMenu.etc = tmp.etc;
                                newOrderedMenu.count = tmp.count;
                                newOrderedMenu.price = tmp.price;
                                mOrderList.add(newOrderedMenu);
                            }
                        }
                        setOrderedListView();
//                        getMyLocation();
                        getPhoneNumber();
                        mTextPrice.setText((mImageButton1.isSelected() ? "* " : "") +mUtils.setComma(calcTotalPrice()) + "원");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        } else {
            finish();
        }
    }


    /**
     * 위도,경도로 주소구하기
     *
     * @param lat
     * @param lng
     * @return 주소
     */
    public String getAddress(Context mContext, double lat, double lng) {
        String nowAddress = "현재 위치를 확인 할 수 없습니다.";
        Geocoder geocoder = new Geocoder(mContext, Locale.KOREA);
        List<Address> address;
        try {
            if (geocoder != null) {
                //세번째 파라미터는 좌표에 대해 주소를 리턴 받는 갯수로
                //한좌표에 대해 두개이상의 이름이 존재할수있기에 주소배열을 리턴받기 위해 최대갯수 설정
                address = geocoder.getFromLocation(lat, lng, 1);

                if (address != null && address.size() > 0) {
                    // 주소 받아오기
                    String currentLocationAddress = address.get(0).getAddressLine(0).toString();
                    nowAddress = currentLocationAddress;

                }
            }

        } catch (IOException e) {
            Toast.makeText(mContext, "주소를 가져 올 수 없습니다.", Toast.LENGTH_LONG).show();

            e.printStackTrace();
        }
        return nowAddress;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        LogUtil.d("퍼미션 허용여부 requestCode:" + requestCode);
        LogUtil.d("퍼미션 허용여부 permissions:" + permissions);
        LogUtil.d("퍼미션 허용여부 grantResults:" + grantResults[0]);
        switch (requestCode) {
            case REQUEST_ADDRESS: {
                getMyLocationProcess();
                break;
            }
            case REQUEST_PHONENUMBER: {
                if(grantResults[0] == 0)    //허용
                    getPhoneNumberProcess();
                else
                    finish();
                break;
            }
        }
    }

    /**
     * 유저의 휴대폰 번호를 불러옴
     ***/
    private void getPhoneNumber() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("주문내역 확인을 위해 회원님의 전화번호를 불러올 수 있도록 권한 승인을 해주셔야 합니다.")
                    .setOkButton("확인", view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONENUMBER);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    }).setCancelButton("취소", view -> {
                        MessageDialogBuilder.getInstance(this).dismiss();
                        finish();

                    })
                    .complete();

        } else {
            //이미 승인완료 따라서 번호 가져올것.
            String result = getPhoneNumberProcess();
            if(result == null)
            {
                //연락처가 유효하지 않음. 따라서 주문 제한
                MessageDialogBuilder.getInstance(this)
                        .setTitle("알림")
                        .setContent("회원님의 휴대전화의 번호를 불러올 수 없어 주문이 제한됩니다.")
                        .onDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                finish();
                            }
                        })
                        .setOkButton("확인", view -> {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONENUMBER);
                            MessageDialogBuilder.getInstance(this).dismiss();
                        })
                        .complete();
            }
        }
    }

    private String getPhoneNumberProcess() {
        String myNumber = null;
        TelephonyManager mgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            myNumber = mgr.getLine1Number();
            myNumber = myNumber.replace("+82", "0");
            myNumber = myNumber.replace("-", "");
            this.mEditPhone.setText(myNumber + "");


            /**
             * 혹시 내쪽에 최근 주문한 건수가 2건이상인지 확인
             * **/

            FirebaseDatabase.getInstance().getReference().child("order").child(myNumber).orderByChild("state").endAt(1)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            LogUtil.d("최근 주문 건 확인 : " + dataSnapshot);
                            LogUtil.d("최근 주문 건수 확인 : " + dataSnapshot.getChildrenCount());
                            if (dataSnapshot != null && dataSnapshot.getChildrenCount() >= 2) {
                                mFlagTwoOrdered = true;
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

            getMyLocation();
        } catch (Exception e) {
            LogUtil.d("불러올 수 없음 : " + e);

            myNumber = null;
        }
        finally {
            return myNumber;
        }
    }

    /**
     * 사용자의 위치를 수신
     */
    private void getMyLocation() {

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 사용자 권한 요청
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ADDRESS);
            MessageDialogBuilder.getInstance(this)
                    .setTitle("알림")
                    .setContent("편리한 주소입력을 위해 회원님의 위치를 확인합니다. 권한 승인후 이용해주세요.")
                    .setDefaultButtons(view -> {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PHONENUMBER);
                        MessageDialogBuilder.getInstance(this).dismiss();
                    })
                    .complete();
        } else {

            getMyLocationProcess();
            // 수동으로 위치 구하기
//            String locationProvider = LocationManager.GPS_PROVIDER;
//            currentLocation = locationManager.getLastKnownLocation(locationProvider);
//            if (currentLocation != null) {
//                double lng = currentLocation.getLongitude();
//                double lat = currentLocation.getLatitude();
//                Log.d("Main", "longtitude=" + lng + ", latitude=" + lat);
//                LogUtil.d("주소지 가져요기 : " + getAddress(this, lat, lng));
//                this.mEditAddress.setText(getAddress(this, lat, lng) + " ");
//                this.mEditAddress.setSelection(mEditAddress.getText().length());
//                Toast.makeText(this, "나머지 주소를 입력해주세요.", Toast.LENGTH_SHORT).show();
//
//            }
        }
    }


    private void getMyLocationProcess() {
        LogUtil.d("주소지 가져요기 : getMyLocationProcess");

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location currentLocation = null;
        // 수동으로 위치 구하기
        String locationProvider = LocationManager.GPS_PROVIDER;
        String locationProvider2 = LocationManager.NETWORK_PROVIDER;
        LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    double lng = location.getLongitude();
                    double lat = location.getLatitude();
                    Log.d("Main", "longtitude=" + lng + ", latitude=" + lat);
                    LogUtil.d("주소지 가져요기 : " + getAddress(mContext, lat, lng));
                    mEditAddress.setText(getAddress(mContext, lat, lng) + " ");
                    mEditAddress.setSelection(mEditAddress.getText().length());
                    mEditAddress.requestFocus();
                    Toast.makeText(mContext, "나머지 주소를 입력해주세요.", Toast.LENGTH_SHORT).show();
                    locationManager.removeUpdates(this);
                    mLoadingView.hide();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                mLoadingView.hide();

            }

            @Override
            public void onProviderEnabled(String s) {
                mLoadingView.hide();

            }

            @Override
            public void onProviderDisabled(String s) {
                mLoadingView.hide();

            }
        };


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "위치 정보를 불러올 수 없습니다. 직접 입력해주세요.", Toast.LENGTH_SHORT).show();
            mLoadingView.hide();
        } else {

            locationManager.requestLocationUpdates(locationProvider, 0, 0, listener);
            locationManager.requestLocationUpdates(locationProvider2, 0, 0, listener);


//            locationManager.requestSingleUpdate(locationProvider, new LocationListener() {
//                @Override
//                public void onLocationChanged(Location location) {
//                    if (location != null) {
//                    double lng = location.getLongitude();
//                    double lat = location.getLatitude();
//                    Log.d("Main", "longtitude=" + lng + ", latitude=" + lat);
//                    LogUtil.d("주소지 가져요기 : " + getAddress(mContext, lat, lng));
//                    mEditAddress.setText(getAddress(mContext, lat, lng) + " ");
//                    mEditAddress.setSelection(mEditAddress.getText().length());
//                    Toast.makeText(mContext, "나머지 주소를 입력해주세요." , Toast.LENGTH_SHORT).show();
//                }
//                }
//
//                @Override
//                public void onStatusChanged(String s, int i, Bundle bundle) {
//
//                }
//
//                @Override
//                public void onProviderEnabled(String s) {
//
//                }
//
//                @Override
//                public void onProviderDisabled(String s) {
//
//                }
//            }, null);


//            locationManager.getLastKnownLocation(locationProvider);
//            if (currentLocation != null) {
//                double lng = currentLocation.getLongitude();
//                double lat = currentLocation.getLatitude();
//                Log.d("Main", "longtitude=" + lng + ", latitude=" + lat);
//                LogUtil.d("주소지 가져요기 : " + getAddress(this, lat, lng));
//                this.mEditAddress.setText(getAddress(this, lat, lng) + " ");
//                this.mEditAddress.setSelection(mEditAddress.getText().length());
//                Toast.makeText(this, "나머지 주소를 입력해주세요." , Toast.LENGTH_SHORT).show();
//            }
        }
    }


    private int calcTotalPrice() {
        if (this.mOrderList != null && this.mOrderList.size() >= 1) {
            int total = 0;
            for (OrderedMenu menu : this.mOrderList) {
                total += (menu.count * menu.price);
            }


            if(this.mImageButton1.isSelected())
                return (int) (total + mDeliveryFee);


            return total;
        }
        return 0;
    }


    private void setOrderedListView() {
        if (this.mOrderList != null && this.mOrderList.size() >= 1) {
            for (OrderedMenu menu : this.mOrderList) {
                OrderedMenuListItem item = new OrderedMenuListItem(this);
                item.setData(
                        menu.title,
                        menu.count,
                        mUtils.setComma(menu.price) + "원"
                );
                this.mlayoutReceipt.addView(item);
            }
        }
    }


    private void clickItemProcess() {
        if (this.mImageButton1.isSelected()) {
            this.mLayoutUserInfo.setVisibility(View.VISIBLE);
            this.mTextDeliveryFee.setVisibility(View.VISIBLE);
        } else {
            this.mLayoutUserInfo.setVisibility(View.GONE);
            this.mTextDeliveryFee.setVisibility(View.INVISIBLE);


        }
        this.mTextPrice.setText((this.mImageButton1.isSelected() ? "* " : "") +this.mUtils.setComma(calcTotalPrice()) + "원");

    }


}
