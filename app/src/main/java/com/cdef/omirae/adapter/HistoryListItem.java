package com.cdef.omirae.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cdef.omirae.R;
import com.cdef.omirae.data.History;
import com.cdef.omirae.data.OrderedMenu;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.Query;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class HistoryListItem extends FirebaseListAdapter<History> {


    public HistoryListItem(Context context, ObservableSnapshotArray<History> dataSnapshots, @LayoutRes int modelLayout) {
        super(context, dataSnapshots, modelLayout);
    }

    public HistoryListItem(Context context, SnapshotParser<History> parser, @LayoutRes int modelLayout, Query query) {
        super(context, parser, modelLayout, query);
    }

    public HistoryListItem(Context context, Class<History> modelClass, @LayoutRes int modelLayout, Query query) {
        super(context, modelClass, modelLayout, query);


    }



    @Override
    protected void populateView(View v, History model, int position) {


        LogUtil.d("아이템 갯수1 : " + model.menu.size());

        for(Iterator<OrderedMenu> list = model.menu.iterator(); list.hasNext();)
        {
            if(list.next() == null)
            {
                list.remove();
            }
        }

        LogUtil.d("아이템 갯수2 : " + model.menu.size());

        ((TextView) v.findViewById(R.id.text_title)).setText(
                (model.menu.size() > 1  ? model.menu.get(0).title + " 외 " + (model.menu.size()-1)+"건" : model.menu.get(0).title)
        );
        ((TextView) v.findViewById(R.id.text_date)).setText(Utils.convertDate("yyyy년 MM월 dd일 HH시 mm분", Utils.calcDate(model.time)));


        switch ((int) model.state)
        {
            case 1:
                ((TextView) v.findViewById(R.id.text_state)).setText("주문 접수중");
                ((TextView) v.findViewById(R.id.text_state)).setTextColor(Color.parseColor("#DF4D4D"));

                break;
            case 0:
                ((TextView) v.findViewById(R.id.text_state)).setText("주문 확인 및 처리중");
                ((TextView) v.findViewById(R.id.text_state)).setTextColor(Color.parseColor("#DF4D4D"));

                break;
            case 2:
                ((TextView) v.findViewById(R.id.text_state)).setText("완료");
                ((TextView) v.findViewById(R.id.text_state)).setTextColor(Color.parseColor("#000000"));

                break;
        }


        ((TextView) v.findViewById(R.id.text_state)).setText((model.state == 1 ? "주문 접수중" : model.state == 0? "주문 확인 및 처리중" : "배달 완료"));
        ((TextView) v.findViewById(R.id.text_type)).setText((model.delivery == 0 ? "배달 주문" : model.delivery == 1? "방문 포장" : "매장 식사"));

        LogUtil.d("가져온 목록 state: " + model.state);
        LogUtil.d("가져온 목록 model: " + model);
        LogUtil.d("가져온 목록 model.menu.size: " + model.menu.size());

        for(OrderedMenu item : model.menu)
        {
            LogUtil.d("- item: " + item);

            if(item != null)
                LogUtil.d("- item.title: " + item.title);

        }

    }







}
