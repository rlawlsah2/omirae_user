package com.cdef.omirae.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;

import com.cdef.omirae.R;
import com.cdef.omirae.activity.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class Utils {

    public Utils()
    {

    }

    /**
     * 1000단위 콤마
     * **/
    public String setComma(long data)
    {
        int result = Integer.parseInt(data + "");
        return new java.text.DecimalFormat("#,###").format(result);
    }


    /**
     * dp -> pixel 변환
     * @param context
     * @param dp dp값
     * return int 픽셀로 변환된 값
     * ***/
    public int dpToPx(Context context, int dp) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float px = dp * (metrics.densityDpi / 160f);

        return (int) px;
    }
    /**
     * pixel -> dp 변환
     * @param context
     * @param px px 값
     * return int dp로 변환된 값
     * ***/
    public int pxToDp(Context context, int px) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float dp = px / (metrics.densityDpi / 160f);

        return (int) dp;

    }


    public String getCurrentTime()
    {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss");
        String sDate = formatter2.format(cal.getTime()); // for file-name


        /**
         * firebase 때문에 9999999999에 뺀값을 리턴해준다.
         * **/

        long corrector = Long.parseLong("99999999999999");
        long oDate = Long.parseLong(sDate);

        return String.valueOf((corrector - oDate));
    }

    public static String calcDate(String input)
    {
        /**
         * firebase 때문에 9999999999에 뺀값을 리턴해준다.
         * **/

        long corrector = Long.parseLong("99999999999999");
        long oDate = Long.parseLong(input);

        return String.valueOf((corrector - oDate));
    }


    public static void orderComplete(Context context, String title)
    {

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("주문완료")
                .setContentText(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(99099 /* ID of notification */, notificationBuilder.build());
    }

    public static String convertDate(String newFormat, String time)
    {
        SimpleDateFormat currentTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat returnTimeFormat = new SimpleDateFormat(newFormat);



        try {
            Date inputTime = currentTimeFormat.parse(time);
            String result = returnTimeFormat.format(inputTime);
            return result;

        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }

    }


}
