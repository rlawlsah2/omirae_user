package com.cdef.omirae.util;

import android.os.Bundle;
import android.util.Log;

/**
 * Created by kimjinmo on 2017. 2. 1..
 */

public class LogUtil {

    public static void d(String str)
    {
        Log.d("kk9999", str);
    }
    public static String bundle2string(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        String string = "Bundle{";
        for (String key : bundle.keySet()) {
            string += " " + key + " => " + bundle.get(key) + ";";
        }
        string += " }Bundle";
        return string;
    }
}
