package com.cdef.omirae.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.cdef.omirae.R;
import com.cdef.omirae.activity.CartActivity;
import com.cdef.omirae.activity.HistoryDetailActivity;
import com.cdef.omirae.activity.HistoryListActivity;
import com.cdef.omirae.activity.MenuDetailActivity;
import com.cdef.omirae.activity.OrderActivity;
import com.cdef.omirae.data.History;
import com.cdef.omirae.data.Menu;

/**
 * Created by kimjinmo on 2016. 11. 16..
 */

public class NavigationUtils {

    /***
     * 주문 액티비티
     * **/
    public static void goOrderActivity(Context context)
    {
        Intent intent = new Intent(context, OrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("type","cart");
        ((Activity)context).startActivityForResult(intent, 99);
        ((Activity)context).overridePendingTransition(0,0);
    }
    /***
     * 주문 액티비티 - 바로주문인 경우
     * **/
    public static void goOrderActivity(Context context, Menu menu, int count)
    {
        Intent intent = new Intent(context, OrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("title",menu.title);
        intent.putExtra("description",menu.description);
        intent.putExtra("image",menu.image);
        intent.putExtra("etc",menu.etc);
        intent.putExtra("price",menu.price);
        intent.putExtra("count",count);
        intent.putExtra("type","direct");
        ((Activity)context).startActivityForResult(intent, 99);
        ((Activity)context).overridePendingTransition(0,0);
    }
    public static void goMenuDetailActivity(Context context, Menu menu)
    {
        Intent intent = new Intent(context, MenuDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("seq",menu.seq);
        intent.putExtra("title",menu.title);
        intent.putExtra("description",menu.description);
        intent.putExtra("image",menu.image);
        intent.putExtra("etc",menu.etc);
        intent.putExtra("price",menu.price);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
    public static void goCartActivity(Context context)
    {
        Intent intent = new Intent(context, CartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
    public static void goHistoryListActivity(Context context)
    {
        Intent intent = new Intent(context, HistoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
    public static void goHistoryDetailActivity(Context context, History history)
    {
        Intent intent = new Intent(context, HistoryDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("history", history);
        context.startActivity(intent);
        ((Activity) context).overridePendingTransition(0,0);

    }
}
