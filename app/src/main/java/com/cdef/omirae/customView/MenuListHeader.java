package com.cdef.omirae.customView;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cdef.omirae.OmiraeApplication;
import com.cdef.omirae.R;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.NavigationUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuListHeader extends LinearLayout {


    @BindView(R.id.image_background)
    ImageView mImageBackground;
    @BindView(R.id.text_name)
    TextView mTextName;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    @BindView(R.id.button_call)
    Button mButtonCall;
    @OnClick(R.id.button_call)
    void goCall(View v) {
        NavigationUtils.goCartActivity(getContext());
    }

    @BindView(R.id.button_history_)
    Button mButtonHistory;
    @OnClick(R.id.button_history_)
    void goHistory(View v) {
        LogUtil.d("주문내역 보기");
        NavigationUtils.goHistoryListActivity(getContext());
    }

    @BindView(R.id.layout_content)
    RelativeLayout mLayoutContent;

    public MenuListHeader(Context context) {
        super(context);
        registerHandler();
    }

    public MenuListHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        registerHandler();
    }

    public MenuListHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        registerHandler();
    }



    private void registerHandler() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_menu_list_header, this, true);
        ButterKnife.bind(this);

    }

    public void setImage(String url)
    {
        Glide.with(getContext()).load(url)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE).into(this.mImageBackground);

    }


}
