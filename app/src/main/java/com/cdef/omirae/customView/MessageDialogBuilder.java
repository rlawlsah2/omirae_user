package com.cdef.omirae.customView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cdef.omirae.OmiraeApplication;
import com.cdef.omirae.R;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class MessageDialogBuilder extends Dialog {

    @BindView(R.id.Alert_Title)
    TextView mTextTitle;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.Alert_Content)
    TextView mTextContent;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.Alert_CANCLE)
    TextView mButtonCancel;
    @BindView(R.id.Alert_OK)
    TextView mButtonOK;
    @BindView(R.id.Alert_3rd_button)
    TextView mButton3rd;


    ////멤버변수
    private Context mContext;
    private static MessageDialogBuilder mInstance;
    private Utils mUtils = new Utils();
    ////

    public static MessageDialogBuilder getInstance(Context context) {
        if (MessageDialogBuilder.mInstance != null) {
            MessageDialogBuilder.mInstance.dismiss();
        }

        if (MessageDialogBuilder.mInstance == null) {
            MessageDialogBuilder.mInstance = new MessageDialogBuilder(context);
        }

        MessageDialogBuilder.mInstance.init(context);
        return MessageDialogBuilder.mInstance;
    }

    public static void clearInstance() {
        MessageDialogBuilder.mInstance = null;
    }


    private MessageDialogBuilder(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    private MessageDialogBuilder(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private MessageDialogBuilder(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_messageview);
        ButterKnife.bind(this);
        ///1. 윈도우 셋팅
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        int calcWidth = (int) (displayMetrics.widthPixels * 0.9);
        params.width = (calcWidth > this.mUtils.dpToPx(mContext, 320) ? calcWidth : this.mUtils.dpToPx(mContext, 320));
        this.getWindow().setAttributes((WindowManager.LayoutParams) params);

        ///2. 하위 뷰 셋팅
    }

    public MessageDialogBuilder init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        this.mTextTitle.setText("알림");
        this.mTextContent.setText("내용");
        this.mButtonCancel.setText("취소");
        this.mButtonCancel.setVisibility(View.GONE);
        this.mButton3rd.setText("기타");
        this.mButton3rd.setVisibility(View.GONE);
        this.mButtonOK.setText("확인");
        this.mButtonOK.setVisibility(View.GONE);

        return this;
    }

    public MessageDialogBuilder setTitle(String title) {
        this.mTextTitle.setText(title);
        return this;
    }

    public MessageDialogBuilder setContent(String content) {
        this.mTextContent.setText(content);
        return this;
    }

    public MessageDialogBuilder setContent(Spanned content) {
        this.mTextContent.setText(content);
        return this;
    }

    public MessageDialogBuilder setCancelButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButtonCancel.setText(title);
        this.mButtonCancel.setOnClickListener(listener);
        this.mButtonCancel.setVisibility(View.VISIBLE);
        return this;
    }

    public MessageDialogBuilder setCancelButton() {

        this.mButtonCancel.setText("취소");
        this.mButtonCancel.setOnClickListener(view -> {
            this.dismiss();
        });
        this.mButtonCancel.setVisibility(View.VISIBLE);
        return this;
    }

    public MessageDialogBuilder setOkButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButtonOK.setText(title);
        this.mButtonOK.setOnClickListener(listener);
        this.mButtonOK.setVisibility(View.VISIBLE);

        return this;
    }

    public MessageDialogBuilder set3rdButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButton3rd.setText(title);
        this.mButton3rd.setOnClickListener(listener);
        this.mButton3rd.setVisibility(View.VISIBLE);

        return this;
    }

    public MessageDialogBuilder setDefaultButton() {
        this.mButtonOK.setOnClickListener(view -> dismiss());
        this.mButtonOK.setVisibility(View.VISIBLE);
        return this;
    }
    /***
     * 취소버튼은 기본, 확인버튼만 람다로
     ***/
    public MessageDialogBuilder setDefaultButtons(View.OnClickListener listener) {
        this.mButtonCancel.setOnClickListener(view -> dismiss());
        this.mButtonCancel.setVisibility(View.VISIBLE);
        this.mButtonOK.setOnClickListener(listener);
        this.mButtonOK.setVisibility(View.VISIBLE);
        return this;
    }


    public MessageDialogBuilder onDismissListener(OnDismissListener listener)
    {
        this.setOnDismissListener(listener);
        return this;
    }
    @Override
    public void dismiss() {
        super.dismiss();
        MessageDialogBuilder.mInstance = null;
    }

    public void complete() {
        LogUtil.d("MessageDialogBuilder 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

}
