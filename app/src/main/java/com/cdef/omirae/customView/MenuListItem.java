package com.cdef.omirae.customView;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.cdef.omirae.R;
import com.cdef.omirae.data.Menu;
import com.cdef.omirae.util.NavigationUtils;
import com.cdef.omirae.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuListItem extends LinearLayout {


    Context mContext;
    Utils mUtils = new Utils();

    @BindView(R.id.image1)
    ImageView mImage1;
    @BindView(R.id.text1)
    TextView mText1;
    @BindView(R.id.layout1)
    LinearLayout mLayout1;
    @OnClick(R.id.layout1)
    void clickLayout1(View v)
    {
        if(menu1 != null)
        {
            NavigationUtils.goMenuDetailActivity(mContext, menu1);

//            NavigationUtils.goOrderActivity(mContext);
//            Toast.makeText(getContext(), "선택한 메뉴는 : " + menu1.title, Toast.LENGTH_SHORT).show();
        }
    }

    @BindView(R.id.image2)
    ImageView mImage2;
    @BindView(R.id.text2)
    TextView mText2;
    @BindView(R.id.layout2)
    LinearLayout mLayout2;
    @OnClick(R.id.layout2)
    void clickLayout2(View v)
    {
        if(menu2 != null)
        {
//            Toast.makeText(getContext(), "선택한 메뉴는 : " + menu2.title, Toast.LENGTH_SHORT).show();
            NavigationUtils.goMenuDetailActivity(mContext, menu2);
        }
    }

    private Menu menu1 = null;
    private Menu menu2 = null;

    public MenuListItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public MenuListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;

        registerHandler();
    }

    public MenuListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;

        registerHandler();
    }


    private void registerHandler() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_menu_list_item, this, true);
        ButterKnife.bind(this);
    }

    public void setData(Menu menu)
    {
        if(menu1 == null)
        {
            ///메뉴1 셋팅
            this.menu1 = menu;
//            Glide.with(getContext()).load(menu1.image).into(this.mImage1);

            Glide.with(getContext()).load(menu1.image)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .override(mUtils.dpToPx(getContext(), 100), mUtils.dpToPx(getContext(), 100))
                     .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            mImage1.setImageBitmap(resource);
                        }
                    });
            this.mText1.setText(menu.title +"\n"+ mUtils.setComma(menu.price) + "원");
            this.mLayout1.setVisibility(View.VISIBLE);




        }
        else if(menu2 == null)
        {
            ///메뉴2 셋팅
            this.menu2 = menu;
//            Glide.with(getContext()).load(menu2.image).into(this.mImage2);

            Glide.with(getContext()).load(menu2.image)
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .override(mUtils.dpToPx(getContext(), 100), mUtils.dpToPx(getContext(), 100))
                     .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            mImage2.setImageBitmap(resource);
                        }
                    });
            this.mText2.setText(menu.title +"\n"+ mUtils.setComma(menu.price) + "원");
            this.mLayout2.setVisibility(View.VISIBLE);

        }

    }


}
