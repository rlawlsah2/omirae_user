package com.cdef.omirae.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.omirae.R;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class CountButton extends LinearLayout {


    Context mContext;
    @BindView(R.id.button_minus)
    Button mButtonMinus;
    @OnClick(R.id.button_minus)
    void clickMinus(View v)
    {

        int count = Integer.parseInt(this.mTextCount.getText().toString());
        LogUtil.d("clickMinus : " + count);

        if(count > 1)
        {
            this.mTextCount.setText(String.valueOf(count-1));
        }


        setButtonsState();
    }
    @BindView(R.id.text_count)
    TextView mTextCount;
    @OnTextChanged(value = R.id.text_count, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void textChange(CharSequence s, int start, int before, int count)
    {
        LogUtil.d("카운트 변환 : " + mTextCount.getText());
        listener.countChange();
    }



    @BindView(R.id.button_plus)
    Button mButtonPlus;
    @OnClick(R.id.button_plus)
    void clickPlus(View v)
    {
        int count = Integer.parseInt(this.mTextCount.getText().toString());
        LogUtil.d("clickMinus : " + count);
        this.mTextCount.setText(String.valueOf(count+1));
        setButtonsState();
    }

    Utils mUtils = new Utils();

    private eventListener listener;

    private void setButtonsState()
    {
        if(this.mTextCount.getText().toString() != null)
        {

            int count = Integer.parseInt(this.mTextCount.getText().toString());
            if(count <=1)
            {
                this.mButtonMinus.setEnabled(false);
            }
            else
            {
                this.mButtonMinus.setEnabled(true);
            }
        }

        LogUtil.d("카운팅 버튼 상태 mButtonMinus: " + this.mButtonMinus.isEnabled());
        LogUtil.d("카운팅 버튼 상태 mButtonPlus: " + this.mButtonPlus.isEnabled());

    }



    public CountButton(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public CountButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CountButton);
        registerHandler(typedArray);
    }

    public CountButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CountButton, defStyleAttr, 0);
        registerHandler(typedArray);
    }


    private void registerHandler(TypedArray typedArray) {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_count_button, this, true);
        ButterKnife.bind(this);


        /////attrs를 기반으로 뷰를 셋팅한다
        if (typedArray != null) {

            int buttonSize = typedArray.getDimensionPixelSize(R.styleable.CountButton_buttonsSize, 30);
            int buttonBackgroundColor = typedArray.getColor(R.styleable.CountButton_buttonColor, mContext.getResources().getColor(R.color.white));
            int buttonTextColor = typedArray.getColor(R.styleable.CountButton_buttonTextColor, mContext.getResources().getColor(R.color.black));
            int textColor = typedArray.getColor(R.styleable.CountButton_textColor, mContext.getResources().getColor(R.color.black));
            float textSize = typedArray.getDimension(R.styleable.CountButton_textSize, 15);
            textSize = mUtils.pxToDp(mContext, (int) textSize);


            LogUtil.d("버튼 buttonSize : " + buttonSize);
            LogUtil.d("버튼 buttonBackgroundColor : " + buttonBackgroundColor);
            LogUtil.d("버튼 buttonTextColor : " + buttonTextColor);
            LogUtil.d("버튼 textColor : " + textColor);
            LogUtil.d("버튼 textSize : " + textSize);

            if (buttonSize != 0) {
                LayoutParams buttonLP = (LayoutParams) this.mButtonMinus.getLayoutParams();
                buttonLP.height = buttonSize;
                buttonLP.width = buttonSize;
                this.mButtonMinus.setLayoutParams(buttonLP);



                LayoutParams buttonLP2 = (LayoutParams) this.mButtonPlus.getLayoutParams();
                buttonLP2.height = buttonSize;
                buttonLP2.width = buttonSize;
                this.mButtonPlus.setLayoutParams(buttonLP2);


                LayoutParams textLP = (LayoutParams) this.mTextCount.getLayoutParams();
                textLP.height = buttonSize;
                this.mTextCount.setLayoutParams(textLP);

            }
            else
            {
                LayoutParams buttonLP = (LayoutParams) this.mButtonMinus.getLayoutParams();
                buttonLP.height = 20;
                buttonLP.width = 20;
                this.mButtonMinus.setLayoutParams(buttonLP);
                this.mButtonPlus.setLayoutParams(buttonLP);
            }


            this.mButtonMinus.setBackgroundColor(buttonBackgroundColor);
            this.mButtonMinus.setTextColor(buttonTextColor);
            this.mButtonPlus.setBackgroundColor(buttonBackgroundColor);
            this.mButtonPlus.setTextColor(buttonTextColor);
            this.mTextCount.setTextColor(textColor);
            this.mTextCount.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);


            ///전체적인 layout의 크기를 조절한다



//            LayoutParams layoutLP = (LayoutParams) this.getLayoutParams();
//            layoutLP.height = buttonSize;
//            this.setLayoutParams(layoutLP);




        } else {

        }
    }

    public void setListener(eventListener listener)
    {
        this.listener = listener;
    }

    public int getCount()
    {
        try {
            return Integer.parseInt(this.mTextCount.getText().toString());
        }
        catch (NumberFormatException e)
        {
            return 0;
        }
    }
    public void setCount(String count)
    {
        this.mTextCount.setText(count);
        setButtonsState();
    }

    public interface eventListener
    {
        void countChange();
    }
}
