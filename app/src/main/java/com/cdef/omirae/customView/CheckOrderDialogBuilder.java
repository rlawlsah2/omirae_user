package com.cdef.omirae.customView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cdef.omirae.R;
import com.cdef.omirae.util.LogUtil;
import com.cdef.omirae.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class CheckOrderDialogBuilder extends Dialog {

    @BindView(R.id.Alert_Title)
    TextView mTextTitle;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.content)
    LinearLayout content;
    @BindView(R.id.Alert_CANCLE)
    TextView mButtonCancel;
    @BindView(R.id.Alert_OK)
    TextView mButtonOK;
    @BindView(R.id.Alert_3rd_button)
    TextView mButton3rd;
    @BindView(R.id.text_type)
    TextView mTextType;
    @BindView(R.id.text_address)
    TextView mTextAddress;
    @BindView(R.id.text_phone)
    TextView mTextPhone;


    ////멤버변수
    private Context mContext;
    private static CheckOrderDialogBuilder mInstance;
    private Utils mUtils = new Utils();
    ////

    public static CheckOrderDialogBuilder getInstance(Context context) {
        if (CheckOrderDialogBuilder.mInstance != null) {
            CheckOrderDialogBuilder.mInstance.dismiss();
        }

        if (CheckOrderDialogBuilder.mInstance == null) {
            CheckOrderDialogBuilder.mInstance = new CheckOrderDialogBuilder(context);
        }

        CheckOrderDialogBuilder.mInstance.init(context);
        return CheckOrderDialogBuilder.mInstance;
    }

    public static void clearInstance() {
        CheckOrderDialogBuilder.mInstance = null;
    }


    private CheckOrderDialogBuilder(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    private CheckOrderDialogBuilder(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private CheckOrderDialogBuilder(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_check_order);
        ButterKnife.bind(this);
        ///1. 윈도우 셋팅
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        WindowManager.LayoutParams params = this.getWindow().getAttributes();
        int calcWidth = (int) (displayMetrics.widthPixels * 0.95);
        params.width = (calcWidth > this.mUtils.dpToPx(mContext, 330) ? calcWidth : this.mUtils.dpToPx(mContext, 330));
        this.getWindow().setAttributes((WindowManager.LayoutParams) params);

        ///2. 하위 뷰 셋팅
    }

    public CheckOrderDialogBuilder init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        this.mTextTitle.setText("주문확인");
        this.mButtonCancel.setText("취소");
        this.mButtonCancel.setVisibility(View.GONE);
        this.mButton3rd.setText("기타");
        this.mButton3rd.setVisibility(View.GONE);
        this.mButtonOK.setText("주문");
        this.mButtonOK.setVisibility(View.GONE);

        return this;
    }

    public CheckOrderDialogBuilder setTitle(String title) {
        this.mTextTitle.setText(title);
        return this;
    }

    public CheckOrderDialogBuilder setContent(long type, String address, String phone) {
        this.mTextType.setText((type == 0? "배달 주문" : (type == 1 ? "방문 포장" : "매장 식사")));
        if(type == 0)
        {
            this.mTextAddress.setText(address);
            this.mTextAddress.setVisibility(View.VISIBLE);
        }
        else
        {
            this.mTextAddress.setVisibility(View.GONE);
        }
        this.mTextPhone.setText(phone);
        return this;
    }

    public CheckOrderDialogBuilder setCancelButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButtonCancel.setText(title);
        this.mButtonCancel.setOnClickListener(listener);
        this.mButtonCancel.setVisibility(View.VISIBLE);
        return this;
    }

    public CheckOrderDialogBuilder setCancelButton() {

        this.mButtonCancel.setText("취소");
        this.mButtonCancel.setOnClickListener(view -> {
            this.dismiss();
        });
        this.mButtonCancel.setVisibility(View.VISIBLE);
        return this;
    }

    public CheckOrderDialogBuilder setOkButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButtonOK.setText(title);
        this.mButtonOK.setOnClickListener(listener);
        this.mButtonOK.setVisibility(View.VISIBLE);

        return this;
    }

    public CheckOrderDialogBuilder set3rdButton(String title, View.OnClickListener listener) {

        if (title != null)
            this.mButton3rd.setText(title);
        this.mButton3rd.setOnClickListener(listener);
        this.mButton3rd.setVisibility(View.VISIBLE);

        return this;
    }

    public CheckOrderDialogBuilder setDefaultButton() {
        this.mButtonOK.setOnClickListener(view -> dismiss());
        this.mButtonOK.setVisibility(View.VISIBLE);
        return this;
    }

    /***
     * 취소버튼은 기본, 확인버튼만 람다로
     ***/
    public CheckOrderDialogBuilder setDefaultButtons(View.OnClickListener listener) {
        this.mButtonCancel.setOnClickListener(view -> dismiss());
        this.mButtonCancel.setVisibility(View.VISIBLE);
        this.mButtonOK.setOnClickListener(listener);
        this.mButtonOK.setVisibility(View.VISIBLE);
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        CheckOrderDialogBuilder.mInstance = null;
    }

    public void complete() {
        LogUtil.d("MessageDialogBuilder 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public CheckOrderDialogBuilder onDismissListener(OnDismissListener listener)
    {
        this.setOnDismissListener(listener);
        return this;
    }


    @Override
    public void onBackPressed() {
        this.mButtonCancel.performClick();
    }
}
