package com.cdef.omirae.customView;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cdef.omirae.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class OrderedMenuListItem extends LinearLayout {


    @BindView(R.id.text_title)
    TextView mTextTitle;
    @BindView(R.id.text_count)
    TextView mTextCount;
    @BindView(R.id.text_price)
    TextView mTextPrice;

    public OrderedMenuListItem(Context context) {
        super(context);
        registerHandler();
    }

    public OrderedMenuListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        registerHandler();
    }

    public OrderedMenuListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        registerHandler();
    }


    private void registerHandler() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_ordered_menu_list_item, this, true);
        ButterKnife.bind(this);
    }

    public void setData(String title, int count, String price) {
        this.mTextTitle.setText(title);
        this.mTextCount.setText(String.valueOf(count)+"개");
        this.mTextPrice.setText(price);

    }


}
