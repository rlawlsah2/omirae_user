package com.cdef.omirae.customView;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.cdef.omirae.R;
import com.cdef.omirae.data.Cart;
import com.cdef.omirae.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjinmo on 2017. 3. 21..
 * <p>
 * 식당 상세보기 페이지 내 메뉴 아이템
 */

public class CartItemLayout extends LinearLayout {

    @BindView(R.id.button_delete)
    TextView mButtonDelete;
    @BindView(R.id.text_price)
    TextView mTextPrice;
    @BindView(R.id.count_button)
    CountButton mCountButton;
    @BindView(R.id.image)
    ImageView mImage;


    @OnClick(R.id.button_delete)
    void deleteItem(View v) {
//        Toast.makeText(mContext, "해당 아이템을 삭제합니다 : " + this.mData.getTicketCode(), Toast.LENGTH_SHORT).show();

        MessageDialogBuilder.getInstance(mContext)
                .setTitle("삭제")
                .setContent(this.mData.title + " 항목을 삭제하시겠습니까?")
                .setDefaultButtons(view -> {
                    listener.onDelete(this.mData);
                    MessageDialogBuilder.getInstance(mContext).dismiss();
                })
                .complete();

    }

    @BindView(R.id.text_restaurant_name)
    TextView mTextRestaurantName;
    @BindView(R.id.text_menu_title)
    TextView mTextMenuTitle;
    @BindView(R.id.text_menu_limit)
    TextView mTextMenuLimit;
    @BindView(R.id.layout_main)
    LinearLayout mLayoutMain;
    private Context mContext;
    private int mCount = 1;

    private Cart mData;
    private Utils mUtils = new Utils();

    public CartItemLayout(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public CartItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        registerHandler();
    }

    public CartItemLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        registerHandler();
    }

    public void setData(Cart data) {
        this.mData = data;
        setView();
    }

    private void setView() {
        if (this.mData != null) {

            this.mTextMenuTitle.setText(mData.title);
            this.mTextPrice.setText(this.mUtils.setComma(mData.price) + "원");
            Glide.with(mContext).load(mData.image).asBitmap()
                    .override(mUtils.dpToPx(getContext(), 100), mUtils.dpToPx(getContext(), 100))
                    .into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    mImage.setImageBitmap(resource);
                }
            });
            this.mCountButton.setListener(new CountButton.eventListener() {
                @Override
                public void countChange() {
                    mData.count = mCountButton.getCount();
                    if(listener != null)
                        listener.onCountChange(mData.seq, mData.count);
                }
            });
            this.mCountButton.setCount(mData.count + "");

//            this.mTextRestaurantName.setText(mData.getStoreName());
//            this.mTextMenuLimit.setText(mExString.getTimeLimit(mData.getExpiresAt(), "yyyy년 MM월 dd일 HH:mm까지"));
//            this.mTextMenuTitle.setText(mData.getMenuName());
//            this.mTextPrice.setText(this.mExString.setComma(mData.getPrice()) + "원");
//            this.mCount = mData.getCount();
//            LogUtil.d("처음 셋팅할때 : " + this.mCount);
//            this.mTextCount.setText(this.mCount + "매");
//
//            this.mButtonPlus.setEnabled(true);
//            if(this.mCount == 1)
//            {
//                this.mButtonMinus.setEnabled(false);
//            }
//            else
//            {
//                this.mButtonMinus.setEnabled(true);
////            }
        }
    }


    public interface ButtonClickListener {
        void onCountChange(long seq, int count);
        void onDelete(Cart cart);
    }

    private ButtonClickListener listener;

    public void setButtonClickListener(ButtonClickListener listener) {
        this.listener = listener;
    }

    /**
     * injection 할때 호출되는 함수
     **/
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        registerHandler();
    }

    private void registerHandler() {
        LayoutInflater.from(this.mContext).inflate(R.layout.layout_cart_item, this, true);
        ButterKnife.bind(this);

//        this.mCountButton.setListener();

    }


}
