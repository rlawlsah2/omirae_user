package com.cdef.omirae.data;

/**
 * Created by kimjinmo on 2017. 9. 4..
 */

public class Default {

    public long version;
    public String phoneNum;
    public String homePic;
    public long deliveryFee;
    public long open;   //0 : 영업종료, 1: 영업중
    public String openSub;
}
