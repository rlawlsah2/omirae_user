package com.cdef.omirae.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 25..
 *
 * 주문 확정된 데이터가 포함된다.
 */


public class Order implements Serializable {

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String address;
    public String time;

    public Order()
    {

    }

}
