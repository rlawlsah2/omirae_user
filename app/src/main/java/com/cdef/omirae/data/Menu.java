package com.cdef.omirae.data;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class Menu {

    public long seq;
    public String title;
    public String description;
    public long price;
    public String image;
    public String etc;


    public Menu()
    {

    }


    public void setData(long seq, String title, String description, long price, String image, String etc)
    {
        this.seq = seq;
        this.title = title;
        this.description = description;
        this.price = price;
        this.image = image;
        this.etc = etc;

    }


}
