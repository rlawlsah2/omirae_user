package com.cdef.omirae.data;

/**
 * Created by kimjinmo on 2017. 8. 25..
 */

public class OrderState {

    public long state = 1;  //1: 주문 확인중, 0: 주문 접수 처리중, 2: 주문 처리완료
    public long delivery = 0;  //0: 배달주문, 1: 방문 포장, 2: 매장 식사
    public String phone;
    public String address;
    public String time;
    public String memo;
    public long deliveryFee;
    public OrderState()
    {

    }
}
