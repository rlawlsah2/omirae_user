package com.cdef.omirae.data;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 28..
 */
@IgnoreExtraProperties
public class History implements Serializable {

    public long state;
    public String address;
    public String time;
    public String memo;
    public long delivery;
    public long deliveryFee;

    //    public Order history;
    public ArrayList<OrderedMenu> menu;

    public History()
    {

    }
}
