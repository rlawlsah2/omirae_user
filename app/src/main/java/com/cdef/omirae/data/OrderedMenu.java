package com.cdef.omirae.data;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

@IgnoreExtraProperties
public class OrderedMenu implements Serializable {

    public int count;
    public long seq;
    public String title;
    public String description;
    public long price;
    public String image;
    public String etc;

    public OrderedMenu()
    {

    }

}
